<?php

require_once 'vendor/autoload.php';
require_once './app/Settings.php';
require_once './app/models/Error.php';
require_once './app/models/History.php';

use Bliive\API\App;

date_default_timezone_set('UTC');

// Run app
$app = (new Bliive\API\App())->get();

//Execute the Slim application
$app->run();