<?php

namespace Bliive\API;

/**
 * Settings Class.
 * This class is used to set the container configuration for the Slim Framework.
 */
class Settings
{

    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\Container
     */
    private $container;

    public function __construct() {

        // Load the basic settings
        $config = ['settings' => ['displayErrorDetails' => ! false]];

        // Create the container based on the configs
        $container = new \Slim\Container($config); // Create the container container


        // When the router does not have the router set.
        $container['notFoundHandler'] = function ($c) {
            return function ($request, $response) use ($c) {
                return $c['response']->withJson(new Error(404, "API Command.", "File or folder not found at the specified path. Make sure you are on the right URL."), 404, 128);
            };
        };

        // When application has a route that matches the current HTTP request URI but NOT the HTTP request method
        $container['notAllowedHandler'] = function ($c) {
            return function ($request, $response, $methods) use ($c) {
                return $c['response']->withJson(new Error(405, "Not Allowed. Request method not expected.", 'Allowed methods for this URL: ' . implode(', ', $methods)), 405, 128);
            };
        };

        $this->container = $container;

    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\Container
     */
    public function get()
    {
        return $this->container;
    }

}