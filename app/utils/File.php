<?php

namespace Bliive\API;

/**
 * File Class.
 * This class is used to save the history into the files.
 */
class File {

    /**
     * Save the history into a JSON and CSV file
     */
    public static function saveHistory($history) {

        // Save to the JSON database
        $jsonHistory = $history->toJson();
        $currentData = file_get_contents('./database/history.json'); 
        $arrayData = json_decode($currentData, true);  
        $arrayData[] = $jsonHistory; 
        $finalData = json_encode($arrayData, 128);  
        file_put_contents('./database/history.json', $finalData);

        // Save to the CSV database
        $columnSeparator = ";";
        $csvLine = $history->getCreatedDate() .$columnSeparator . $history->getDddOrigin() .$columnSeparator .$history->getDddDestiny()  .$columnSeparator .$history->getTimeSpent() .$columnSeparator ."Fale Mais " .$history->getPlanMinutes() .$columnSeparator .($history->getConsumeWithPlan() ? "$" .$history->getConsumeWithPlan() : "-") .$columnSeparator . ($history->getConsumeWithoutPlan() ? "$" .$history->getConsumeWithoutPlan() : "-") .PHP_EOL;
        $fp = fopen('./database/history.csv', 'a');
        fwrite($fp, $csvLine);
        fclose($fp);
    }
}