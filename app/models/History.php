<?php

namespace Bliive\API;

/**
 * History Class.
 * This class is used by the API to output the history.
 */
class History implements \JsonSerializable  {

	/** @var \DateTime $createdDate the actual date */
	private $createdDate;
	
	/** @var string $dddOrigin the DDD of origin in string format, for example 018  */
	private $dddOrigin;
	
	/** @var string $dddDestiny the DDD of destiny in string format, for example 011  */
	private $dddDestiny;
	
	/** @var int $timeSpent the time spent in minutes, for example 20  */
	private $timeSpent;
	
	/** @var string $planMinutes the type of plan used in the call, for example 1 for the FaleMais 30 plan */
	private $planMinutes;
	
	/** @var double $consumeWithPlan the cost consume with the FaleMais plan  */
	private $consumeWithPlan;
	
	/** @var double $consumeWithoutPlan the cost consume without the FaleMais plan */
	private $consumeWithoutPlan;

	/**
	 * Creates an history object
	 */
	public function __construct($createdDate, $dddOrigin, $dddDestiny, $timeSpent, $planMinutes) {
		$this->setCreatedDate($createdDate);
		$this->setDddOrigin($dddOrigin);
		$this->setDddDestiny($dddDestiny);
		$this->setTimeSpent($timeSpent);
		$this->setPlanMinutes($planMinutes);
		
		$this->setConsume($planMinutes);
	}

	/**
     * Get The history createdDate.
     *
     * @return \DateTime the createdDate
     */
	public function getCreatedDate() {
		return $this->createdDate;
	}

	/**
     * Set the history created date in d-m-Y H:i:s format
     *
     * @param \DateTime $createdDate
     *            the actual date.
     * @return \DateTime the history createdDate object
     */
    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate !== NULL ? $createdDate->format('d-m-Y H:i:s') : NULL;
        return $this;
    }


	/**
	 * Get the ddd origin
	 *
	 * @return string the ddd origin string
	 */
	public function getDddOrigin() {
		return $this->dddOrigin;
	}

	/**
	 * Set the ddd origin. If the ddd origin contains more than 3 character or is not a number, then throw an InvalidArgumentException
	 *
	 * @param string $dddOrigin
	 */
	public function setDddOrigin($dddOrigin) {
		if(strlen($dddOrigin) > 3 || !is_numeric($dddOrigin) || (int) $dddOrigin < 0) {
			throw new \InvalidArgumentException("The Origin DDD needs to be a number and cannot be longer than 3 characters");
		}
		$this->dddOrigin = trim($dddOrigin);
		return $this;
	}

	/**
	 * Get the ddd destiny
	 *
	 * @return string the $dddDestiny string
	 */
	public function getDddDestiny() {
		return $this->dddDestiny;
	}

	/**
	 * Set the ddd Destiny. If the ddd of destiny contains more than 3 character or is not a number, then throw an InvalidArgumentException
	 *
	 * @param string the $dddDestiny string
	 */
	public function setDddDestiny($dddDestiny) {
		if(strlen($dddDestiny) > 3 || !is_numeric($dddDestiny) || (int) $dddDestiny < 0 ) {
			throw new \InvalidArgumentException("The Destiny DDD needs to be a number and cannot be longer than 3 characters");
		}
		$this->dddDestiny = trim($dddDestiny);
		return $this;
	}

	/**
	 * Get the time spent in the call in minutes
	 *
	 * @return integer the time spent
	 */
	public function getTimeSpent() {
		return $this->timeSpent;
	}

	/**
	 * Set the time spent in the call in minutes. If the time is below zero, then throw an InvalidArgumentException
	 *
	 * @return integer 
	 */
	public function setTimeSpent($timeSpent) {
		if($timeSpent < 0){
			throw new \InvalidArgumentException("The time spent in the must be an integer number bigger or equal to 0");
		}
		$this->timeSpent = $timeSpent;
		return $this;
	}

	/**
	 * Get the plan minutes in integer format
	 *
	 * @return int the plan minutes
	 */
	public function getPlanMinutes() {
		return $this->planMinutes;
	}

	/**
	 * Set the plan in minutes format, for each number between 1 and 3 it will represent each FaleMais plan. 
	 * If the number provided is not 1, 2 or 3, then return InvalidArgumentException
	 *
	 * @return int the plan minutes
	 */
	public function setPlanMinutes($planMinutes) {
		switch ($planMinutes) {
			case 1:
				$this->planMinutes = 30;
				break;
			case 2:
				$this->planMinutes = 60;
				break;
			case 3:
				$this->planMinutes = 120;
				break;
			default:
				throw new \InvalidArgumentException("The plan needs to be a number between 1 and 3, representing respectively FaleMais 30, FaleMais 60 and FaleMais 120");
	
		}
		return $this;
	}

	
	/**
	 * Get the cost of the consume with the FaleMais plan
	 *
	 * @return double cost of the consume with the FaleMais plan
	 */
	public function getConsumeWithPlan() {
		return $this->consumeWithPlan;
		
	}

	/**
	 * Set the cost of the consume with the FaleMais plan
	 *
	 * @return double the cost of the consume with the FaleMais plan
	 */
	public function setConsumeWithPlan($consumeWithPlan) {
		$this->consumeWithPlan = $consumeWithPlan;
		return $this;
	}

	
	/**
	 * Get the cost of the consume without the FaleMais plan
	 *
	 * @return double the cost of the consume without the FaleMais plan
	 */
	public function getConsumeWithoutPlan() {
		return $this->consumeWithoutPlan;
	}

	/**
	 * Set the cost of the consume without the FaleMais plan
	 *
	 * @return double the cost of the consume without the FaleMais plan
	 */
	public function setConsumeWithoutPlan($consumeWithoutPlan) {
		$this->consumeWithoutPlan = $consumeWithoutPlan;
		return $this;
	}

	/**
	 * Calculate the cost of the consume 
	 * 
	 */
	public function setConsume($plan) {

		//Read from JSON rates file
        $json = file_get_contents('./database/rates.json');

		$rates= json_decode($json, true);

		foreach($rates as $item) {
			
			//Check if the origin and destiny DDD is equal from the one provided 
			if($item["dddOrigin"] == $this->getDddOrigin() && $item["dddDestiny"] == $this->getDddDestiny()) {

				//Get the cost per minute of the call based on the origin and destiny DDD provided
				$dddCost = $item["cost"];

				//Get the remaining minutes from the time consumed based on the plan minutes
				$minutesWithFaleMais =   $this->getTimeSpent() - $this->getPlanMinutes();

				//Set the cost of the consume with the plan active
				$this->setConsumeWithPlan($minutesWithFaleMais <= 0 ? NULL : (($dddCost+ ($dddCost * 10) / 100)) * $minutesWithFaleMais) ;

				//Set the cost of the consume without the plan active
				$this->setConsumeWithoutPlan($dddCost * $this->getTimeSpent());

				break;
			}
		}	
	}

	/**
	 * Allow serialize this object as a JSON string.
	 * This will let us encode even the private fields.
	 *
	 * @return Object members to be serialized.
	 */
	public function toJson() {
		$vars = get_object_vars($this);
		return $vars ;
	}

	/**
	 * Allow serialize this object as a JSON string.
	 * This will let us encode even the private fields.
	 *
	 * @return Object members to be serialized.
	 */
	public function serializeJson() {
		$vars = get_object_vars($this);
		unset($vars["createdDate"]);
		return $vars;
	}

	/**
	 * Allow serialize this object as a JSON string.
	 * This will let us encode even the private fields.
	 *
	 * @return Object members to be serialized.
	 */
	public function JsonSerialize() {
		$vars = get_object_vars($this);
		return $vars;
	}
}
