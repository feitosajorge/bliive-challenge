<?php

namespace Bliive\API;

include './app/utils/File.php';

/**
 * App Class.
 * This class is used to create the API commands.
 */
class App
{
    
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;

    public function __construct() {

        $container = (new Settings())->get();

        // Start the APP
        $app = new \Slim\App($container);

        /**
         * Redirect the users that access the root of the API to the documentation
         */
        $app->GET('/', function ($request, $response, $args) {
            return $response->withRedirect("/docs", 301);
        });

        /**
         * Summary: Create the Call request
         * Notes: Return the cost with and without the FaleMais plan of the call based on the origin and destiny DDD, the time spent on the call and the plan used.
         * Example of call: /call?origin=011&destiny=017&time=80&plan=2
         * Output-Formats: [application/json]
         */
        $app->GET('/call',
            function ($request, $response, $args) {

                // Get the query params
                $queryParams = $request->getQueryParams();

                $origin = isset($queryParams['origin']) ?  $queryParams['origin'] : NULL;
                $destiny = isset($queryParams['destiny']) ? $queryParams['destiny'] : NULL;
                $time = isset($queryParams['time']) ? (int) $queryParams['time'] : NULL;
                $plan = isset($queryParams['plan']) ? (int) $queryParams['plan'] : NULL;
                
                // Check if one of the params was not passed in the call
                if (! $queryParams || ! $origin || ! $destiny || ! $time || ! $plan) {
                    return $response->withJson(new Error(403, "Bad API Request.", "You need to provide all the parameters necessary."), 403, 128);
                }

                $history = NULL;
                try {
                    // Create the history with the costs of the call simulated
                    $history = new History(new \DateTime(), $origin, $destiny, $time, $plan);
                } catch (\Exception $e) {
                    return $response->withJson(new Error(403, "Bad API Data Request.", $e->getMessage()), 403, 128);
                }

                // Save to the JSON database
                try {
                    File::saveHistory($history);
                } catch (\Exception $e) {
                    return $response->withJson(new Error(500, "Internal Server Error.", $exception->getMessage()), 500, 128);
                }

                // Return the value in JSON format
                return $response->withJson($history->serializeJson(), 200, 128);
        });

        /**
         * Summary: Display the full or filtered history in JSON format
         * Notes: Return the calls history in JSON format, in can be filtered by date for example: ?date=03-08-2018
         * If the date is not compatible with the "d-m-Y" or "d-m-Y H:i:s" format then the list will return null
         * Output-Formats: [application/json]
         */
        $app->GET('/history',
            function ($request, $response, $args) {

                //Get the params
                $queryParams = $request->getQueryParams();
                
                $date = isset($queryParams['date']) ?  $queryParams['date'] : NULL;
                
                //Read from JSON history file
                $file = file_get_contents('./database/history.json');
                $jsonFile = json_decode($file ? $file : NULL, true);

                //If the date was not provided of file is null
                if(! $date || ! $jsonFile) {
                    return $response->withJson($jsonFile , 200, 128);
                }

                //Filter the results by date
                $jsonArray = [];
                foreach($jsonFile as $item) {
                    if(strpos($item["createdDate"], $date) !== false) {
                        $jsonArray[] = $item;
                    }
                }

                //Return the History JSON array formated in the screen
                return $response->withJson($jsonArray , 200, 128);
            });

        /**
         * Summary: Display the full history in CSV format with the headers
         * Notes: Use this command to get the complete history of all calls in CSV format with the headers included, spliting each column by semicollon
         * Output-Formats: [text/plain]
         */
        $app->GET('/history-csv',
            function ($request, $response, $args) {
                //Read from CSV history file
                $csvBody = nl2br(file_get_contents('./database/history.csv'));

                $csvHeaders = nl2br("Data/Hora; Origem; Destino; Tempo; Plano FaleMais; Com FaleMais; Sem FaleMais\r\n");

                //Return the full history CSV in the screen
                return $response->getBody()->write($csvHeaders .$csvBody, 200);
            });

        /**
         * Summary: Display the rates based on each DDD available
         * Notes: Return in JSON format the rates for each DDD available
         * Output-Formats: [application/json]
         */
        $app->GET('/rates',
            function ($request, $response, $args) {
                //Read from JSON rates file
                $json = file_get_contents('./database/rates.json');

                $json_decode = json_decode($json);

                //Return the rates json formated in the screen
                return $response->withJson($json_decode, 200, 128);
            });


        /**
         * Summary: Display the Swagger document in JSON format
         * Notes: The user can get the JSON documentation and transform into the yaml format using the http://editor.swagger.io/ website
         * Output-Formats: [application/json]
         */
        $app->GET('/docs',
            function ($request, $response, $args) {
                //Read from JSON docs file
                $json = file_get_contents('./documentation/docs.json');

                $json_decode = json_decode($json);

                //Return the docs json formated in the screen
                return $response->withJson($json_decode, 200, 128);
            });

        $this->app = $app;
    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}