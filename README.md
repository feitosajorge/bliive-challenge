# Bliive Challenge

### Projeto focado para verificação e validação de habilidades quanto ao desenvolvimento de algoritmos

[Link da descrição do desafio](https://bitbucket.org/tiagobliive/bliive-back-end-test/src/master/README.md) 

#### Tecnologias:

- PHP
- Slim Framework >= 3.0
- PHPUnit 5.4.*
- Swagger 


#### Requisitos:

- [PHP 5.6.37](http://php.net/downloads.php) (caso ainda não o tenha, siga os passos abaixo)
	1. Há dois arquivos do PHP compactados dentro da pasta do projeto */bliive-challenge/softwares*
	2. Para o Windows, extraia os arquivos do *php-5.6.37-Win32-VC11-x86.zip* para dentro de uma pasta na raiz do seu sistema, por exemplo: *C:/* 
	3. Ponha a sua pasta root contendo os arquivos PHP, por exemplo *C:/php*, para a variável de ambiente do seu sistema
	4. Execute no seu console o comando **php** e verifique se funciona

#### Instruções para instalar e executar o projeto:

 - Execute no console dentro da raiz do projeto */bliive-challenge* os seguintes comandos:
 	1. `php composer.phar install`
		- Caso o comando tenha falhado, verifique se a pasta do PHP contém um arquivo chamado *php.ini*, caso não tenha, renomeie o *php.ini-development* para *php.ini*
		- Ative a extensão php_openssl.dll desmarcando o ponto e vírgula dentro do arquivo *php.ini* 
		- Execute o comando novamente
 	2. ` php -S localhost:8080 `
	 	- O número porta pode ser mudado, podendo ser localhost:80
 - Acesse no seu browser: `localhost:8080`


#### Alguns comandos da API disponíveis:

- http://localhost:8080
- http://localhost:8080/history
- http://localhost:8080/history?date=03-08-2018
- http://localhost:8080/history-csv
- http://localhost:8080/rates
- http://localhost:8080/docs

- http://localhost:8080/call?origin=011&destiny=017&time=59&plan=2
- http://localhost:8080/call?origin=011&destiny=016&time=20&plan=1
- http://localhost:8080/call?origin=018&destiny=011&time=200&plan=3
- http://localhost:8080/call?origin=018&destiny=017&time=100&plan=1


## Execute os testes:

- Execute na pasta root */bliive-challenge* o seguinte comando: 
	- `./vendor/bin/phpunit ./test/.`
	- `./vendor/bin/phpunit --testdox ./test/.`
	- `./vendor/bin/phpunit ./test/GeneralTest.php`
	- `./vendor/bin/phpunit ./test/CallTest.php`
	
```
PHPUnit 5.4.8 by Sebastian Bergmann and contributors.

..................                                                18 / 18 (100%)

Time: 572 ms, Memory: 3.75MB

OK (18 tests, 60 assertions)
```

## Swagger:

- Verifique a documentação em JSON ou YALM no [Swagger Editor](http://editor.swagger.io/)


