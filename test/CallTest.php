<?php

use Bliive\API\App;

require_once './app/models/Error.php';
require_once './app/models/History.php';

use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * CallTests Class.
 * This class is used to test the call API command.
 */
class CallTest extends PHPUnit_Framework_TestCase
{

    /**
     * Stores an instance of the Slim application.
     */
    protected $app;

    /**
     * Get an instance of the application.
     */
    public function setUp()
    {
        $this->app = (new App())->get();
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 011
     *      DDD destiny: 017
     *      Time Spent in the call: 80
     *      Plan used: 2
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testCallGetSuccessCaseOne() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=011&destiny=017&time=80&plan=2',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["dddOrigin"], "011");
        $this->assertSame($result["dddDestiny"], "017");
        $this->assertSame($result["timeSpent"], 80);
        $this->assertSame($result["planMinutes"], 60);
        $this->assertSame($result["consumeWithPlan"], 37.40);
        $this->assertSame($result["consumeWithoutPlan"], 136);
    } 

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 011
     *      DDD destiny: 016
     *      Time Spent in the call: 20
     *      Plan used: 1
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testCallGetSuccessCaseTwo() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=011&destiny=016&time=20&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["dddOrigin"], "011");
        $this->assertSame($result["dddDestiny"], "016");
        $this->assertSame($result["timeSpent"], 20);
        $this->assertSame($result["planMinutes"], 30);
        $this->assertSame($result["consumeWithPlan"], NULL);
        $this->assertSame($result["consumeWithoutPlan"], 38);
    } 

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: 011
     *      Time Spent in the call: 200
     *      Plan used: 3
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testCallGetSuccessCaseThree() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=011&time=200&plan=3',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["dddOrigin"], "018");
        $this->assertSame($result["dddDestiny"], "011");
        $this->assertSame($result["timeSpent"], 200);
        $this->assertSame($result["planMinutes"], 120);
        $this->assertSame($result["consumeWithPlan"], 167.2);
        $this->assertSame($result["consumeWithoutPlan"], 380);
    } 

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: 017
     *      Time Spent in the call: 100
     *      Plan used: 1
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testCallGetSuccessCasefour() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=017&time=100&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["dddOrigin"], "018");
        $this->assertSame($result["dddDestiny"], "017");
        $this->assertSame($result["timeSpent"], 100);
        $this->assertSame($result["planMinutes"], 30);
        $this->assertSame($result["consumeWithPlan"], null);
        $this->assertSame($result["consumeWithoutPlan"], null);
    } 

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: 017
     *      Time Spent in the call: 100
     *      Plan used: 10
     * Result:
     *      The test shall indentify that the API command will return an error because the plan provided is wrong
     */
    public function testCallGetInvalidPlan() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=017&time=100&plan=10',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The plan needs to be a number between 1 and 3, representing respectively FaleMais 30, FaleMais 60 and FaleMais 120");
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: 017
     *      Time Spent in the call: -234
     *      Plan used: 1
     * Result:
     *      The test shall indentify that the API command will return an error because the time spent provided is below 0
     */
    public function testCallGetInvalidTimeSpent() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=017&time=-234&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The time spent in the must be an integer number bigger or equal to 0");
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 0180
     *      DDD destiny: 017
     *      Time Spent in the call: 100
     *      Plan used: 1
     * Result:
     *      The test shall indentify that the API command will return an error because the Origin DDD needs to be a number and cannot be longer than 3 characters
     */
    public function testCallGetInvalidLimitationInOrigin() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=0180&destiny=017&time=100&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The Origin DDD needs to be a number and cannot be longer than 3 characters");
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: a0
     *      DDD destiny: 017
     *      Time Spent in the call: 100
     *      Plan used: 1
     * Result:
     *      The test shall indentify that the API command will return an error because the Origin DDD needs to be a number and cannot be longer than 3 characters
     */
    public function testCallGetInvalidLetterInOrigin() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=a0&destiny=017&time=100&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The Origin DDD needs to be a number and cannot be longer than 3 characters");
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: a01
     *      Time Spent in the call: 100
     *      Plan used: 1
     * Result:
     *      The test shall indentify that the API command will return an error because the Destiny DDD needs to be a number and cannot be longer than 3 characters
     */
    public function testCallGetInvalidLetterInDestiny() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=a01&time=100&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The Destiny DDD needs to be a number and cannot be longer than 3 characters");
    }

    /**
     * Test the Call API command
     * Test case: 
     *      DDD origin: 018
     *      DDD destiny: 12301
     *      Time Spent in the call: 100
     *      Plan used: 1
     * Result:
     *      The test shall indentify that the API command will return an error because the Destiny DDD needs to be a number and cannot be longer than 3 characters
     */
    public function testCallGetInvalidLimitationInDestiny() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/call?origin=018&destiny=12301&time=100&plan=1',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 403);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["solution"], "The Destiny DDD needs to be a number and cannot be longer than 3 characters");
    }

    
}