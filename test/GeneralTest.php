<?php

use Bliive\API\App;

require_once './app/models/Error.php';
require_once './app/models/History.php';

use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * GeneralTest Class.
 * This class is used to test general API commands.
 */
class GeneralTest extends PHPUnit_Framework_TestCase
{

    /**
     * Stores an instance of the Slim application.
     */
    protected $app;

    /**
     * Get an instance of the application.
     */
    public function setUp()
    {
        $this->app = (new App())->get();
    }

    /**
     * Test the Home API command
     * Test case: 
     *      test the root command
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testHomeGet() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 301);
        $this->assertSame((string) $response->getBody(), "");
    } 

    /**
     * Test the history API command
     * Test case: 
     *      test the history command
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testHistoryGet() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/history',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
    }

    /**
     * Test the History API command
     * Test case: 
     *      Date to be filtered = 03-08-2018
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testHistoryWithDateGet() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/history?date=03-08-2018',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
    }

    /**
     * Test the CSV History API command
     * Test case: 
     *      test the csv history command
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testHistoryCsvGet() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/history-csv',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
    }

    /**
     * Test the Docs API command
     * Test case: 
     *      test the docs command
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testDocsGet() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/docs',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["swagger"], "2.0");
    } 

    /**
     * Test an invalid or inexistent API command
     * Test case: 
     *      test an invalid API command: /someurlpath
     * Result:
     *      The test will check that the API command does not exist or could not be found
     */
    public function testInvalidUrl() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/someurlpath',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["message"], "API Command.");
    } 

    /**
     * Test the rates API command
     * Test case: 
     *      test the rates command
     * Result:
     *      The test will check if all the values returns is from what expected
     */
    public function testRatesGet() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/rates',
            ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);    
        $result = json_decode($response->getBody(), true);
        $this->assertNotEmpty($result);
        $this->assertNotEmpty($result[0]);
        $this->assertNotEmpty($result[0]["dddOrigin"]);
        $this->assertNotEmpty($result[0]["dddDestiny"]);
        $this->assertNotEmpty($result[0]["cost"]);
    }


    /**
     * Test if the files are present in the project
     * Test case: 
     *      Check if the rates, history or docs files are present and in the right extension
     * Result:
     *      Confirm that the files are present and that is important exist
     */
    public function testFileExists() {
        $this->assertFileExists('./database/rates.json');
        $this->assertFileExists('./database/history.json');
        $this->assertFileExists('./database/history.csv');
        $this->assertFileExists('./documentation/docs.json');
        $this->assertFileExists('./documentation/docs.yalm');
    }
}